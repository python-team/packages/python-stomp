Source: python-stomp
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Sophie Brun <sophie@freexian.com>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all:any,
 python3-docopt,
 python3-setuptools,
 python3-sphinx,
 python3-websocket
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://github.com/jasonrbriggs/stomp.py
Vcs-Git: https://salsa.debian.org/python-team/packages/python-stomp.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-stomp
Testsuite: autopkgtest-pkg-python

Package: python3-stomp
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-websocket
Suggests: python-stomp-doc
Description: STOMP client library for Python 3
 This package contains stomp.py, a Python client library for accessing messaging
 servers (such as Apollo or RabbitMQ) using the STOMP protocol (versions 1.0,
 1.1 and 1.2). It can also be run as a standalone, command-line client for
 testing.
 .
 This package installs the library for Python 3.

Package: python-stomp-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: Documentation of Python's STOMP client library
 stomp.py is a Python client library for accessing messaging
 servers (such as Apollo or RabbitMQ) using the STOMP protocol (versions 1.0,
 1.1 and 1.2). It can also be run as a standalone, command-line client for
 testing.
 .
 This is the common documentation package.
